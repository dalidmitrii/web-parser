﻿using AngleSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRate
{
    public class HtmlParser
    {
        private List<string> emphasizesList = new List<string>();

        /// <summary>
        /// Возвращает List<string>, содержащий курс валют
        /// </summary>
        public async Task<List<string>> Parse()
        {
            var config = Configuration.Default.WithDefaultLoader();
            var address = "http://cbpmr.net/";

            var document = await BrowsingContext.New(config).OpenAsync(address);
            var emphasize = document.QuerySelectorAll("table tbody tr td h12");



            foreach (var item in emphasize)
            {
                emphasizesList.Add(item.TextContent);
            }

            return emphasizesList;
        }
    }
}

