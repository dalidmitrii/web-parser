﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ExchangeRateProgram
{
    using ExchangeRate;

    class Program
    {
        static void Main(string[] args)
        {
            GetExchangeRate();
            Console.ReadKey();
        }

        public static async void GetExchangeRate()
        {
            HtmlParser htmlParser = new HtmlParser();

            List<string> exchangeRate = new List<string>();
            exchangeRate = await htmlParser.Parse();

            foreach (var item in exchangeRate)
            {
                Console.WriteLine(item);
            }
        }
    }
}
